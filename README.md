GitCPP (Git Commit Push Pull)
=============================

__Author__: Ondrej Sika, <http://ondrejsika.com>, <ondrej@ondrejsika.com>

__GitHub__: <https://github.com/ondrejsika/git-cpp>

__PyPI__: <http://pypi.python.org/pypi/git-cpp>


Changes log
-----------

##### v1.2

Add git init option, if is not a repository

##### v1.1

Add exception if is nothing to commit

##### v1.0

Basic functionality (commit, push, pull)


[![Bitdeli Badge](https://d2weczhvl823v0.cloudfront.net/ondrejsika/git-cpp/trend.png)](https://bitdeli.com/free "Bitdeli Badge")

