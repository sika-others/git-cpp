import os
import sys
import subprocess

class GitCPP:
    def __init__(self, git_path):
        self.git_path = git_path

    def is_repository(self):
        return os.path.exists(os.path.join(self.git_path, ".git"))

    def cmd(self, cmd, force=False):
        if self.is_repository() or force:
            os.chdir(self.git_path)
            subprocess.call(("git %s"%cmd).encode(sys.stdout.encoding), shell=True)

    def changes(self):
        if self.is_repository():
            os.chdir(self.git_path)

            return bool(os.popen("git status --porcelain").read())

    def commit(self, commit_message):
        self.cmd("add -A")
        self.cmd("commit -m \"%s\""%commit_message)

    def init(self):
        self.cmd("init", force=True)

    def push(self):
        self.cmd("push origin master")

    def pull(self):
        self.cmd("pull origin master")
