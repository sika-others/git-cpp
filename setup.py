#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name = "git-cpp",
    version = "1.2.0",
    url = 'https://github.com/ondrejsika/git-cpp/',
    license = 'MIT',
    description = "Git Gui for Commit, Push, Pull",
    author = 'Ondrej Sika',
    author_email = 'ondrej@ondrejsika.com',
    py_modules = ["libgitcpp"],
    scripts = ["git-cpp"],
    include_package_data = True,
    zip_safe = False,
)
